from PIL import Image
import numpy as np
from collections import defaultdict
from Shape import Shape
from ShapeUtil import ShapeUtil
from Generator import Generator
# Your Agent for solving Raven's Progressive Matrices. You MUST modify this file.
#
# You may also create and submit new files in addition to modifying this file.
#
# Make sure your file retains methods with the signatures:
# def __init__(self)
# def Solve(self,problem)
#
# These methods will be necessary for the project's main method to run.

# Install Pillow and uncomment this line to access image processing.
#from PIL import Image
#import numpy

class Agent:
    problemNum = 0
    # The default constructor for your Agent. Make sure to execute any
    # processing necessary before your Agent starts solving problems here.
    #
    # Do not add any variables to this signature; they will not be used by
    # main().
    def __init__(self):
        pass

    # The primary method for solving incoming Raven's Progressive Matrices.
    # For each problem, your Agent's Solve() method will be called. At the
    # conclusion of Solve(), your Agent should return an int representing its
    # answer to the question: 1, 2, 3, 4, 5, or 6. Strings of these ints 
    # are also the Names of the individual RavensFigures, obtained through
    # RavensFigure.getName(). Return a negative number to skip a problem.
    #
    # Make sure to return your answer *as an integer* at the end of Solve().
    # Returning your answer as a string may cause your program to crash.
    def Solve(self,problem):
        try:
            '''Open Problem Figures & Candidate Answers'''
            figureA = openImage('A',problem)
            figureB = openImage('B',problem)
            figureC = openImage('C',problem)
            figures = [figureA,figureB,figureC]
            answer1 = openImage('1',problem)
            answer2 = openImage('2',problem)
            answer3 = openImage('3',problem)
            answer4 = openImage('4',problem)
            answer5 = openImage('5',problem)
            answer6 = openImage('6',problem) 
            answerImages = [answer1,answer2,answer3,answer4,answer5,answer6]
            
            SU = ShapeUtil()
            generator = Generator()

            ''' Use Shape Detector to find Shapes in each figure'''
            shapesInA = SU.findBoldShapes(problem.figures['A'].visualFilename)
            shapesInB = SU.findBoldShapes(problem.figures['B'].visualFilename)
            shapesInC = SU.findBoldShapes(problem.figures['C'].visualFilename)

            shapesInAnswer1 = SU.findBoldShapes(problem.figures['1'].visualFilename)
            shapesInAnswer2 = SU.findBoldShapes(problem.figures['2'].visualFilename)
            shapesInAnswer3 = SU.findBoldShapes(problem.figures['3'].visualFilename)
            shapesInAnswer4 = SU.findBoldShapes(problem.figures['4'].visualFilename)
            shapesInAnswer5 = SU.findBoldShapes(problem.figures['5'].visualFilename)
            shapesInAnswer6 = SU.findBoldShapes(problem.figures['6'].visualFilename)
            answers = [shapesInAnswer1,shapesInAnswer2,shapesInAnswer3,shapesInAnswer4,shapesInAnswer5,shapesInAnswer6]
            
            '''If a shape has been added/removed, iterate through all answers and select the one that has '''
            if(len(shapesInA) < len(shapesInB)):
                 answer = generator.checkAddedShape(shapesInA,shapesInB,shapesInC,answers)
                 return answer + 1
            if(len(shapesInA) < len(shapesInC)):
                answer = generator.checkAddedShape(shapesInA,shapesInC,shapesInB,answers)
                return answer + 1
            
            '''Find the transforms between image sets A->B and A->C'''
            abTransforms = generator.generateAndTest(shapesInA,shapesInB)
            acTransforms = generator.generateAndTest(shapesInA,shapesInC)
            
            '''Check how many of the transforms found exist for each candidate answer. Assign a score to each candidate'''
            totalScore = []
            for i,answer in enumerate(answers):
                cdscore = generator.score(shapesInB,answer,acTransforms)
                bcscore = generator.score(shapesInC,answer,abTransforms)
                total = (cdscore+bcscore)/2
                totalScore.append((total,cdscore,bcscore,i))
            
            '''Based on the list of all scores, determine confidence and select answer'''
            decision = decide(totalScore)
            return decision
        except Exception as e:
         return -1

def decide(totalScore):
    totalScore.sort(key=lambda x: x[0],reverse=True)
    if(totalScore[0][0] > 99):
        return totalScore[0][-1]+1
    elif(totalScore[0][1] > 99 or totalScore[0][2] > 99):
        if(totalScore[1][1] < 99 and totalScore[1][2] < 99):
            return totalScore[0][-1]+1
        else: 
            return -1
    else:
        return -1      


def openImage(key,problem):
    return Image.open(problem.figures[key].visualFilename)