from ShapeUtil import ShapeUtil
from collections import OrderedDict
class Generator:
	''' 
		Class of functions to analyze which transforms exist between shapes in different images.
		Uses the Generate & Test family of AI Algorithms
	'''
	#TODO: Move to ENUM class
	NOOP = 0
	REFLECT_Y = 1
	SHADE = 2

	def __init__(self):
		self.transformations = OrderedDict()
		self.transformations['NO-OP'] = self.noop
		self.transformations['REFLECT_Y'] = self.checkYReflection
		self.transformations['REFLECT_X'] = self.checkXReflection
		self.transformations['SHADE'] = self.shade
		self.SU = ShapeUtil()

	def generateAndTest(self,shapesInA,shapesInB):
		transformList = []
		if(len(shapesInA) > len(shapesInB)):
			numberOfDeletes = len(shapesInA) - len(shapesInB)
			for x in range (0,numberOfDeletes):
				transformList.append(self.delete)
			return transformList

		for shape in shapesInA:
			for transformName,transform in self.transformations.items():
				transformedA = transform(shape)
				if(self.existsInTarget(transformedA,shapesInB)):
					transformList.append(transform)
					break
			return transformList

	def checkAddedShape(self,shapesInA,shapesInB,target,answers):
		addedShape = self.SU.findAddedShape(shapesInA,shapesInB)
		for index,answer in enumerate(answers):
			if(len(answer) == len(target)+1):
				for shapeInAnswer in answer:
					if(self.SU.isSameShape(addedShape.edges,shapeInAnswer.edges)):
						return index
		return -1


	def score(self,sourceShapes,answer,transforms):
		if self.delete in transforms:
			if(len(transforms) + len(answer) == len(sourceShapes)):
				return 100
			else:
				return 0
		numberOfMatchingTransforms = 0
		for shape in answer:
			for transform in transforms:
				if(transform == self.shade):
					transform = self.unshade
				transformedAnswer = transform(shape)
				if(self.existsInTarget(transformedAnswer,sourceShapes)):
					numberOfMatchingTransforms +=1
		return numberOfMatchingTransforms/(len(sourceShapes))*100

	def checkYReflection(self,shape):
		return self.SU.rotateY(shape.edges,[])

	def noop(self,shape):
		return shape.edges

	def checkXReflection(self,shape):
		return self.SU.rptateX(shape.edges,[])
	def unshade(self,shape):
		return self.SU.unshade(shape.edges)
	def shade(self,shape):
		return self.SU.shade(shape.edges)
	def existsInTarget(self,shapeToLookFor,shapesInTarget):
		for shape in shapesInTarget:
			if(self.SU.isSameShape(shapeToLookFor,shape.edges)):
				return True
		return False