from __future__ import division
from PIL import Image,ImageFilter
import numpy as np
import math
from Shape import Shape
import queue
class ShapeUtil:
	'''Class of functions to find shapes and perform transformations on shapes in an image vector'''
	def __init__(self):
		pass

	def findBoldShapes(self,image):
		shapes = []
		imagePath = image
		openImage = Image.open(image).convert('RGB')
		edges = openImage
		pic = edges.load()
		for i in range(25,edges.size[0]-10):
			for j in range(10,edges.size[1]-10):
				'''Once a black pixel is encoutered, visit all connected pixels'''
				if(self.isEdge(pic[i,j],i,j,pic) and not seen((i,j),shapes)): 
					newShapesEdges = self.visitAll(pic,i,j)
					sorted(newShapesEdges)
					'''Create a new shape object and add it to list of known shapes in image'''
					isShaded,transition = self.isShaded(newShapesEdges,Image.open(image).convert('RGB'))
					shape = Shape()
					shape.edges = newShapesEdges
					shape.isShaded = isShaded
					if(not shape.isShaded):
						shape.edges = self.fill(Image.open(image).convert('RGB').load(),transition[1][0],transition[1][1],shape)
					shapes.append(shape)
		return shapes



	def findAddedShape(self,shapesInA,shapesInB):
		'''Check if a shape in image vector A is added to image vector B'''
		if(len(shapesInA) < len(shapesInB)):
			for shape in shapesInB:
				existsInB = False
				for target in shapesInA:
					if((self.isSameShape(shape.edges,target.edges,92,0.89))):
						existsInB = True
						break
				if(existsInB == False):
					return shape

	def shade(self,shape):
		'''Given the area a shape encompasses, flip every pixel to black'''
		transformed = []
		for point in shape:
			color = point[2]
			if(color == 'W'):
				transformed.append((point[0],point[1],'B'))
			else:
				transformed.append((point[0],point[1],point[2]))
		return transformed	

	def unshade(self,shape):
		transformed = []
		for point in shape:
			color = point[2]
			if(color == 'B'):
				transformed.append((point[0],point[1],'W'))
			else:
				transformed.append((point[0],point[1],point[2]))
		return transformed	

	def averageDistanceBetweenPixels(self,shape1,shape2):
		sortedPointsShape1 = sorted(shape1)
		sortedPointsShape2 = sorted(shape2)
		pointsToIterate = 0
		if(len(sortedPointsShape1) > len(sortedPointsShape2)):
			pointsToIterate = len(sortedPointsShape2)
		else:
			pointsToIterate = len(sortedPointsShape1)
		averageDistance = 0
		for x in range(0,pointsToIterate):
			newDistance = math.sqrt((sortedPointsShape1[x][0]-sortedPointsShape2[x][0])**2+(sortedPointsShape1[x][1]-sortedPointsShape2[x][1])**2)
			averageDistance += newDistance
		averageDistance = averageDistance//pointsToIterate
		return averageDistance

	def isSameShape(self,ShapeA,ShapeB,tolerance=93.0,area=0.89):
		numOfPoints = 0
		numOfBlankPoints = 0
		smaller = []
		bigger = []
		if(len(ShapeA) < len(ShapeB)):
			smaller = ShapeA
			bigger = ShapeB
		else:
			smaller = ShapeB
			bigger = ShapeA

		for point in smaller:
			if point in bigger:
				numOfPoints += 1
		if(numOfPoints/len(smaller) *100 > tolerance and len(smaller)/len(bigger) > area):
			return True
		else:
			return False
	
	def isShaded(self,shapeA,img):
		A = img
		Aarr = A.load()
		sortedA = sorted(shapeA)
		shapeA.sort(key=lambda x: x[0])
		lowestX = shapeA[0]
		highestX = shapeA[-1]
		shapeA.sort(key=lambda x: x[1])
		lowestY = shapeA[0]
		highestY = shapeA[-1]
		visit(lowestX[0],lowestX[1],Aarr,shapeA)
		transitions = []
		transitionPoints = [(lowestX[0],lowestY[1])]
		for x in range(0,180-lowestX[0]):
			(computeTransitionPoints(lowestX[0]+x,lowestX[1]+5,lowestX[2],Aarr,shapeA,transitions,transitionPoints))
		return not self.isHollow(transitions),transitionPoints


	def isHollow(self,transformations):
		if(len(transformations) > 2):
			return transformations[0] == "EDGE" and transformations[1] == "WHITE" and transformations[2] == "EDGE"
		else:
			return False

	def fill(self,pic,i,j,shapeA):
		'''Fill an image with white pixels'''
		imageList = shapeA.edges
		if(self.isBlankEdge(pic[i,j],i,j,pic)):
			q = list()
			q.append((i,j))
			while(q):
				i,j = q.pop(-1)
				if(self.isBlankEdge(pic[i+1,j],i+1,j,pic) and (i+1,j) not in imageList):
					q.append((i,j))
					imageList.append((i+1,j,'W'))
				if(self.isBlankEdge(pic[i,j+1],i,j+1,pic) and (i,j+1) not in imageList):
					q.append((i,j+1))
					imageList.append((i,j+1,'W'))
				if(self.isBlankEdge(pic[i+1,j+1],i+1,j+1,pic)):
					q.append((i+1,j+1))
					imageList.append((i+1,j+1,'W'))
				if(self.isBlankEdge(pic[i-1,j],i-1,j,pic)):
					q.append((i-1,j))
					imageList.append((i-1,j,'W'))
				if(self.isBlankEdge(pic[i,j-1],i,j-1,pic)):
					q.append((i,j-1))
					imageList.append((i,j-1,'W'))
				if(self.isBlankEdge(pic[i-1,j-1],i-1,j-1,pic)):
					q.append((i-1,j-1))
					imageList.append((i-1,j-1,'W'))
				if(self.isBlankEdge(pic[i+1,j-1],i+1,j-1,pic)):
					q.append((i+1,j-1))
					imageList.append((i+1,j-1,'W'))	
				if(self.isBlankEdge(pic[i-1,j+1],i-1,j+1,pic)):
					q.append((i-1,j+1))
					imageList.append((i-1,j+1,'W'))
		return imageList

	def rotateY(self,inputShape,filename):
		transformed = []
		for x in range(0,len(inputShape)):
			xCord = inputShape[x][0]
			yCord = inputShape[x][1]
			color = inputShape[x][2]
			if(xCord > 92):
				newXCord = 92 - (xCord-92)
				transformed.append((newXCord,yCord,color))
			if(xCord <= 92):
				newXCord = 92 + (92-xCord)
				transformed.append((newXCord,yCord,color))
		return transformed 


	def rotateX(self,inputShape,filename):
		transformedShape = Shape()
		transformed = []
		for x in range(0,len(inputShape)):
			xCord = inputShape[x][0]
			yCord = inputShape[x][1]
			color = inputShape[x][2]
			if(yCord > 92):
				newYCord = 92 - (yCord-92)
				transformed.append((xCord,newYCord,color))
			if(yCord <= 92):
				newYCord = 92 + (92-yCord)
				transformed.append((xCord,newYCord,color))
		transformedShape.edges = transformed
		return transformed 


	def visitAll(self,pic,i,j):
		'''Runs BFS to find all connected points alongside a pixel to trace through a shape'''
		if(self.isEdge(pic[i,j],i,j,pic)):
			imageList = []
			q = list()
			q.append((i,j))
			while(q):
				i,j = q.pop(-1)
				if(i > 0 and j > 0 and i < 183 and j < 183):
					if(self.isEdge(pic[i+1,j],i+1,j,pic) and (i+1,j) not in imageList):
						q.append((i,j))
						imageList.append((i+1,j,'B'))
					if(self.isEdge(pic[i,j+1],i,j+1,pic) and (i,j+1) not in imageList):
						q.append((i,j+1))
						imageList.append((i,j+1,'B'))
					if(self.isEdge(pic[i+1,j+1],i+1,j+1,pic)):
						q.append((i+1,j+1))
						imageList.append((i+1,j+1,'B'))
					if(self.isEdge(pic[i-1,j],i-1,j,pic)):
						q.append((i-1,j))
						imageList.append((i-1,j,'B'))
					if(self.isEdge(pic[i,j-1],i,j-1,pic)):
						q.append((i,j-1))
						imageList.append((i,j-1,'B'))
					if(self.isEdge(pic[i-1,j-1],i-1,j-1,pic)):
						q.append((i-1,j-1))
						imageList.append((i-1,j-1,'B'))
					if(self.isEdge(pic[i+1,j-1],i+1,j-1,pic)):
						q.append((i+1,j-1))
						imageList.append((i+1,j-1,'B'))	
					if(self.isEdge(pic[i-1,j+1],i-1,j+1,pic)):
						q.append((i-1,j+1))
						imageList.append((i-1,j+1,'B'))							
			return imageList


	def isEdge(self,pixel,i,j,pic):
		if(pixel[0] < 20 and pixel[1] < 20 and pixel[2] < 20):
			pic[i,j] = (255,0,0)
			return True

	def isBlankEdge(self,pixel,i,j,pic):
		if(pixel[0] != 0 and pixel[1] != 0 and pixel[2] != 0):
			pic[i,j] = (0,0,0)
			return True			

def computeTransitionPoints(row,col,color,pic,shape,transitions,transitionPoints):
	'''Some shapes are unshaded, some are shaded, some are partially shaded, and some have subshapes (ex: inscribed cicrles, triangles inside a star).
	   This function helps classify which category a shape belongs to by tracking the number of transitons from black pixel to white pixels
	   that happen inside the area surrounded by a shape. '''
	if((row,col,color) in shape and not transitions):
		transitions.append("EDGE")
	if(not transitions):
		return 
	if((row,col,color) in shape and transitions[-1] == "EDGE"):
		pic[row,col] = (255,0,0)
		return True
	elif(pic[row,col] == (255,255,255) and transitions[-1] == "EDGE"):
		transitions.append("WHITE")
		transitionPoints.append((row,col))
		pic[row,col] = (255,0,0)
	elif(pic[row,col] == (255,255,255) and transitions[-1] == "WHITE"):
		pic[row,col] = (255,0,0)
	elif(pic[row,col] != (255,255,255) and (row,col,color) in shape and transitions[-1] == "WHITE"):
		pic[row,col] = (255,0,0)
		transitions.append("EDGE")

	return False

def seen(currPoint,shapes):
	for shape in shapes:
		if currPoint in shape.edges:
			return True
	return False

def visit(row,col,pic):
	pic[row,col] = (255,0,0)


