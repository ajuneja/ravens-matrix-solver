class Shape:
	def __init__(self):
		self.edges = []
		self.isShaded = False
		self.abTransforms = []
		self.bcTransforms = []

	def setEdges(edges):
		self.edges = edges 